from django.contrib import admin

from home.models import Absen, Classroom

# Register your models here.
admin.site.register(Absen)
admin.site.register(Classroom)
