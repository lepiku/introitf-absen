from django import forms

from .models import Absen

class AbsenForm(forms.Form):
    nama = forms.CharField(max_length=100)
    npm = forms.CharField(max_length=10)

class AbsenModelForm(forms.ModelForm):
    class Meta:
        model = Absen
        fields = ['nama', 'npm']
