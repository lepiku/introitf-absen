from django.shortcuts import render
from django.views import View

from .forms import AbsenForm, AbsenModelForm
from .models import Absen

def index(request):
    absen = Absen.objects.all()

    if request.method == 'POST':
        form = AbsenModelForm(request.POST)
        if form.is_valid():
            # Absen.objects.create(
                # nama=form.data['nama'],
                # npm=form.data['npm'],
            # )
            form.save()
            form = AbsenModelForm()
    else:
        form = AbsenModelForm()

    context = {
        'absen': absen,
        'form': form,
    }

    return render(request, 'index.html', context)
