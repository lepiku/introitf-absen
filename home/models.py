from django.db import models

# Create your models here.
class Absen(models.Model):
    nama = models.CharField(max_length=100)
    npm = models.CharField(max_length=10)
    kelas = models.ForeignKey('Classroom', on_delete=models.SET_NULL,
            default=None, null=True, blank=True)
    teman = models.ManyToManyField('self', blank=True)

    def __str__(self):
        return self.nama

class Classroom(models.Model):
    nama = models.CharField(max_length=100)
    ketua = models.OneToOneField(Absen, on_delete=models.CASCADE)
